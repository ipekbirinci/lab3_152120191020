#include "Clock.h"

#include <iostream>
using namespace std;

namespace Developer1{
void CClock::setTime(const int hours, const int minutes, int seconds)
{
	if (0 <= hours && hours < 24)
		hr = hours;
	else
		hr = 0;

	if (0 <= minutes && minutes < 60)
		min = minutes;
	else
		min = 0;
	
	if (0 <= seconds && seconds < 60)
		sec = seconds;
	else
		sec = 0;
}

void CClock::incrementHours()
{
	hr++;
	if (hr > 23)
		hr=0;
}

void CClock::incrementMinutes()
{
	min++;
	if (min > 59){
		min=0;
		incrementHours(); //increment hours
	}
}

void CClock::incrementSeconds()
{
	sec++;
	if (sec > 59){
		sec=0;
		incrementMinutes(); //increment minutes
	}
}
void CClock::incrementHours()
{
	hr++;
	if (hr > 23)
		hr = 0;
}

bool CClock::equalTime(const CClock& otherClock) const
{
	return (hr == otherClock.hr
		&& min == otherClock.min
		&& sec == otherClock.sec);

}
bool CClock::esitmi(int a, int b, int c, int h, int e, int p)
{
	if (a == h && b == e && c == p)
		return true;
	else
		return false;
}
void CClock::printTime()
{
	if (hr <10)
		cout << "0";
	cout << hr <<":";
	if (min <10)
		cout << "0";
	cout << min <<":";
	if (sec <10)
		cout << "0";
	cout << sec ;
}
}
